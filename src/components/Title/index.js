import React from 'react';

/**
 * HAY 4 TIPOS DIFERENTES DE COMPONENTS:
 * 
 * -Stateless Functional components
 * -Function Components
 * -Components
 * -Pure Components
 * 
 */


const Title = ({ title }) => (
    <h1># {title} #</h1>
)

export default Title;