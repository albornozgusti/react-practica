import React from 'react';


const Child = ({children, legend}) =>(
    <div>
        {children}
        {legend}
    </div>
)

export default Child;