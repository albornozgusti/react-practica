import React from 'react';
import {FormGroup, Label, Input} from'reactstrap';

const InputText = ({ ...props }) => (
    <FormGroup>
        {props.label && <Label>{props.label}</Label>}
        <Input
            type="text"
            {...props}
        /**
         * onChange > cambio de valor
         * value > Nuestro valor
         * className > clases de estilo
         * style > nuestros styles
         */
        />
    </FormGroup>
);

export default InputText;