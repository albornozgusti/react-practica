import React from 'react';
import {FormGroup, Label, Input} from'reactstrap';

const InputEmail = ({ ...props }) => (
    <FormGroup>
        {props.label && <Label>{props.label}</Label>}
        <Input
            type="email"
            {...props}
        /**
         * onChange > cambio de valor
         * value > Nuestro valor
         * className > clases de estilo
         * style > nuestros styles
         */
        />
    </FormGroup>
);

export default InputEmail;