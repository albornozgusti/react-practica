export const FETCH_COUNTRY_REQUESTED = 'FETCH_COUNTRY_REQUESTED';// >>>>> Accion
export const FETCH_COUNTRY_SUCCEEDED = 'FETCH_COUNTRY_SUCCEEDED';// <<<<< Reaccion

export const fetchCountryRequested = () => ({ type: FETCH_COUNTRY_REQUESTED });
export const fetchCountrySucceeded = countries => ({ type: FETCH_COUNTRY_SUCCEEDED, countries });
