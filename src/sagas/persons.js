import { call, put, select } from 'redux-saga/effects';
//import Axios from 'axios';
import PersonsService from '../services/persons';

import { fetchPersonsSucceeded, submitPersonSucceeded, fetchPersonSucceeded, deletePersonSucceeded } from '../actions/persons';


/**
 * metodo call de sagas permite incluir la funcion separado por comas todos los parametros necesarios
 * ej:
 * call(miFuncion, param1, param2, param3, .....etc)
 * function(1,2,3,4)
 * 
 */

export function* fetchPersons({ filter }) {
    const persons = yield call(PersonsService.apiCall, filter)
    console.log('funcion fetchPersons de sagas')
    yield put(fetchPersonsSucceeded(persons))
}

export function* submitPerson() {
    const { currentPersons } = yield select(state => state.persons)
    const { status, data } = yield call(PersonsService.apiSave, currentPersons);
    yield put(submitPersonSucceeded(status, data));

}

export function* fetchPerson({ id }) {
    const person = yield call(PersonsService.apiCallOne, id)
    yield put(fetchPersonSucceeded(person))
}

export function* deletePerson({ id }) {
    yield call(PersonsService.apiDelete, id);
    yield put(deletePersonSucceeded(true));
    yield call(fetchPersons, {});
}