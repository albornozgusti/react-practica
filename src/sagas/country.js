import { call, put } from 'redux-saga/effects';
import Axios from 'axios';

import { fetchCountrySucceeded } from '../actions/country';

const apiCall = async () => {
    const { data, status } = await Axios.get('http://localhost:3001/api/country');
    console.log(data);
    if (status !== 200) {
        return [];
    }

    return data;
}

/**
 * metodo call de sagas permite incluir la funcion separado por comas todos los parametros necesarios
 * ej:
 * call(miFuncion, param1, param2, param3, .....etc)
 * function(1,2,3,4)
 * 
 */

export function* fetchCountry({filter}){
    const countries = yield call(apiCall, filter)
    yield put(fetchCountrySucceeded(countries))
}