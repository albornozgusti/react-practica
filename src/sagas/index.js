//root sagas
import { all, takeEvery } from 'redux-saga/effects';

import { FETCH_COUNTRY_REQUESTED } from '../actions/country'
import { FETCH_PERSONS_REQUESTED, SUBMIT_PERSON_REQUESTED, FETCH_PERSON_REQUESTED, DELETE_PERSON_REQUESTED } from '../actions/persons'
import { fetchCountry } from './country'
import { fetchPersons, submitPerson, fetchPerson, deletePerson } from './persons'

//funcion de escucha
//permiten correr yield <<< es una funcion de llamado
/**
 * funciones static
 * ()*=>{}
 * (*) yield << es una funcion de llamado
 */
export default function* root() {
    yield all([
        //primer parametro es nuestra accion
        //segundo parametro es el sagas a ejecutar
        takeEvery(FETCH_COUNTRY_REQUESTED, fetchCountry),
        takeEvery(FETCH_PERSONS_REQUESTED, fetchPersons),
        takeEvery(SUBMIT_PERSON_REQUESTED, submitPerson),
        takeEvery(FETCH_PERSON_REQUESTED, fetchPerson),
        takeEvery(DELETE_PERSON_REQUESTED, deletePerson)
    ])
}

/**
 * 1 - generar actions y triggers
 * 2 - modificar reducers (parametros objetos)
 * 3 - sagas(en este caso persons)
 * 4 - root (sagas)
 */