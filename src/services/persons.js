import Axios from 'axios';

import HTTP from './http';
export default class Person {

    static async apiCall() {
        console.log('estoy llamando a las personas');
        return HTTP.get('api/persons');
    }

    static async apiCallOne(id) {
        return HTTP.get(`api/persons/${id}`);
    }

    static async apiSave(person) {
        if (person.id) {
            return Axios.put(`http://localhost:3001/api/persons/${person.id}`, person);
        } else {
            return Axios.post('http://localhost:3001/api/persons', person);
        }
    }

    static async apiDelete(id) {
        const { data, status } = await Axios.delete(`http://localhost:3001/api/persons/${id}`);
        console.log(data);
        if (status !== 200) {
            return {};
        }
        return data;
    }

}