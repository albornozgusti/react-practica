import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Title from '../../components/Title';
//import ChildComponent from '../components/ChildComponent';
import Table from '../../components/Table';
import { Container, Row, Col, Button } from 'reactstrap';

import { fetchPersonsRequested, deletePersonRequested } from '../../actions/persons'

class Persons extends Component {
    /*constructor(props) {//siempre que se llame a un constructor
        super(props)//se debe llamar a super
        this.state = {
            documents: []
        }
    }*/

    componentDidMount() {
        this.props.requestPersons();
        // const documents = await fetchPersons();
        // this.props.PersonsSucceeded(documents);
        // //this.setState({ documents })
        // this.forceUpdate();
    }

    render() {
        //const { documents } = this.state;
        const { headers, documents } = this.props;
        return (
            <Container>
                <Row>
                    <Col sm="8">
                        <Title title="Hello world" />
                    </Col>
                    <Col sm="4" className="float-right mt-2">
                        <Button tag={Link} color="warning" to='/persons/new'>Nueva persona</Button>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Table
                            {...{ documents, headers, linkTo: '/persons' }}
                            onDelete={id => this.props.deletePerson(id)}
                        />
                    </Col>
                </Row>

            </Container>
        );
    }
}

/**
 * recibe 3 parametros:
 * mapStateToProps: todo el state del store esta aca, tomamos el store y se lo pasamos a nuestro component como props
 * mapDispatchToProps: idem caso anterior pero todas las acciones que vamos a ejecutar o llamar se la pasan a component como props
 * mergeProps se puede fusionar acciones y propiedades para hacer una sola ej: onChange
 */

/**
 * recibe 2 parametros:
 * 1 - nuestro store / reducer
 * 2 - son todas las propiedades que vienen por herencia
 * o por asignacion
 */
const mapStateToProps = state => ({
    headers: state.persons.headers,
    documents: state.persons.persons
});


/**
 * recibe 2 parametros:
 * 1 - nuestro dispatcher o disparador de acciones
 * 2 - son todas las propiedades que vienen por herencia
 * o por asignacion
 */
const mapDispatchToProps = dispatch => ({
    requestPersons: () => dispatch(fetchPersonsRequested()),
    deletePerson: id => dispatch(deletePersonRequested(id))
});



export default connect(mapStateToProps, mapDispatchToProps)(Persons);
