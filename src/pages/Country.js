import React, { Component } from 'react';
import {connect} from 'react-redux'
import Title from '../components/Title';
//import ChildComponent from '../components/ChildComponent';
import Table from '../components/Table';

import {fetchCountryRequested} from '../actions/country'

class Country extends Component {
    /*constructor(props) {//siempre que se llame a un constructor
        super(props)//se debe llamar a super
        this.state = {
            documents: []
        }
    }*/

    async componentDidMount() {
        this.props.requestCountry();
        // const documents = await fetchCountry();
        // this.props.countrySucceeded(documents);
        // //this.setState({ documents })
        // this.forceUpdate();
    }

    render() {
        //const { documents } = this.state;
        const {headers, documents} = this.props;
        return (
            <div className="App">
                <header className="App-header">
                    <Title title="Hello world" />
                    <div>
                        <Table {...{ documents, headers }} />
                    </div>
                </header>

            </div>
        );
    }
}

/**
 * recibe 3 parametros:
 * mapStateToProps: todo el state del store esta aca, tomamos el store y se lo pasamos a nuestro component como props
 * mapDispatchToProps: idem caso anterior pero todas las acciones que vamos a ejecutar o llamar se la pasan a component como props
 * mergeProps se puede fusionar acciones y propiedades para hacer una sola ej: onChange
 */

 /**
  * recibe 2 parametros:
  * 1 - nuestro store / reducer
  * 2 - son todas las propiedades que vienen por herencia
  * o por asignacion
  */
 const mapStateToProps = state => ({
    headers: state.country.headers,
    documents: state.country.countries
 });


 /**
  * recibe 2 parametros:
  * 1 - nuestro dispatcher o disparador de acciones
  * 2 - son todas las propiedades que vienen por herencia
  * o por asignacion
  */
 const mapDispatchToProps = dispatch =>({
    requestCountry: () => dispatch(fetchCountryRequested()),
 });



export default connect(mapStateToProps, mapDispatchToProps)(Country);
