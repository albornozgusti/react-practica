import {
    FETCH_COUNTRY_REQUESTED,
    FETCH_COUNTRY_SUCCEEDED
} from '../actions/country'

const initialState = {
    countries: [],
    headers: [
        {
            label: 'nombre',
            key: 'name'
        },
        {
            label: 'codigo',
            key: 'code'
        }
    ]
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COUNTRY_REQUESTED:
            return { ...state, countries: [] };
        case FETCH_COUNTRY_SUCCEEDED:
            return { ...state, countries: action.countries };
        default:
            return { ...state };
    }
}